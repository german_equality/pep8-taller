from django.test import TestCase
from nt1.models import circulo

# Create your tests here.

class CirculoTest(TestCase):
    fixtures = ['circulo.json']

    
    def test_area(self):
        circ = circulo.objects.get(id=1)
        self.assertEqual(314, circ.area())

    def test_area2(self):
        circ = circulo.objects.get(id=2)
        self.assertNotEqual(313, circ.area())
    

    def test_perimetro(self):
        circ = circulo.objects.get(id=1)                            # tolerancia de exactitud: hasta 7 decimales
        self.assertAlmostEqual(62.799999951, circ.perimetro())     # within 7 places (5.000000413701855e-08 difference)

    def test_perimetro2(self):
        circ = circulo.objects.get(id=2)
        self.assertNotEqual(120, circ.perimetro())



    def create_circle(self, radio=1):
        return circulo.objects.create(radio=radio)

    def test_create_circle(self):
        c = self.create_circle()
        self.assertTrue(isinstance(c, circulo))
        c.delete()



if __name__ == '__main__':
    CirculoTest.main()