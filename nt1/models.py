from django.db import models
from django.db.models.fields import FloatField, IntegerField
# Create your models here.

PI = 3.14

class circulo(models.Model):

    radio = FloatField()
    


    def __str__(self):
        return "Circulo de radio %s." % self.radio
    
    def area(self):
        return PI*(self.radio)**2
    
    def perimetro(self):
        return 2*PI*self.radio